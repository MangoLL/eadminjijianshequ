
CREATE TABLE `es_tipoffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prosecutorId` int(11) NOT NULL COMMENT '揭发者',
  `defendantId` int(11) DEFAULT NULL COMMENT '被告者',
  `contentType` char(1) NOT NULL DEFAULT '0' COMMENT '0,评论 1，文章，2话题，3用户',
  `contentId` int(11) DEFAULT NULL,
  `report` text COMMENT '补充',
  `status` char(1) DEFAULT '0' COMMENT '0 未处理 1已处理 2搁置',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

INSERT INTO `es_config` ( `name`, `type`, `title`, `group`, `extra`, `describe`, `create_time`, `update_time`, `status`, `value`, `sort`) VALUES ('auto_colse_content', '1', '自动屏蔽内容数量', '3', '', '单位为整数(0:关闭)', '1494222635', '1502504402', '1', '10', '80');
